/*
 * cdda.c: The plugin main source file and main interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda.c,v 1.10 2005/12/17 14:39:27 cm Exp $
 */

#include <vdr/plugin.h>
#include <getopt.h>
#include "audio_playlist.h"
#include "audio_player.h"
#include "cdda_i18n.h"
#include "cdda_menu.h"
#include "cdda_conf.h"
#include "cdda_device.h"
#include "cdda_cddb.h"

static const char *VERSION        = "0.1.0";
static const char *DESCRIPTION    = "music cd player";
static const char *MAINMENUENTRY  = "CD Player";

class cPluginCdda : public cPlugin {
private:
  // Add any member variables or functions you may need here.
public:
  cPluginCdda(void);
  virtual ~cPluginCdda();
  virtual const char *Version(void) { return VERSION; }
  virtual const char *Description(void) { return tr(DESCRIPTION); }
  virtual const char *CommandLineHelp(void);
  virtual bool ProcessArgs(int argc, char *argv[]);
  virtual bool Initialize(void);
  virtual bool Start(void);
  virtual void Stop(void);
  virtual void Housekeeping(void);
  virtual const char *MainMenuEntry(void) { return cCddaConfiguration::GetInstance().allowMenuEntry() ? tr(MAINMENUENTRY) : NULL; }
  virtual cOsdObject *MainMenuAction(void);
  virtual cMenuSetupPage *SetupMenu(void);
  virtual bool SetupParse(const char *Name, const char *Value);
  };

cPluginCdda::cPluginCdda(void)
{
  // Initialize any member variables here.
  // DON'T DO ANYTHING ELSE THAT MAY HAVE SIDE EFFECTS, REQUIRE GLOBAL
  // VDR OBJECTS TO EXIST OR PRODUCE ANY OUTPUT!

  cCddaConfiguration::GetInstance().SetDevice("/dev/cdrom");
}

cPluginCdda::~cPluginCdda()
{
  // Clean up after yourself!
  delete &cCddaCddb::GetInstance();
  delete &cCddaDevice::GetInstance();
  delete &cCddaConfiguration::GetInstance();
}

const char *cPluginCdda::CommandLineHelp(void)
{
  static char *p_help = NULL;

  if (NULL == p_help)
    asprintf(&p_help, "  -d DEVICE,    --device=DEVICE (default: /dev/cdrom)\n"
                      "  -c DIRECTORY, --cddbDir=DIRECTORY");
  return p_help;
}

bool cPluginCdda::ProcessArgs(int argc, char *argv[])
{
  int c;

  static struct option long_options[] = {
    {"device", required_argument, NULL, 'd'},
    {"cddbDir", required_argument, NULL, 'c'},
    {       0,                 0,    0,   0}
  };

  while (-1 != (c = getopt_long(argc, argv, "c:d:", long_options, NULL))) {
    switch (c) {
      case 'c':
        cCddaConfiguration::GetInstance().SetCddbDir(optarg);
        break;
      case 'd':
        cCddaConfiguration::GetInstance().SetDevice(optarg);
        break;
      default:
        return false;
    }
  }
  return true;
}

bool cPluginCdda::Initialize(void)
{
  // Initialize any background activities the plugin shall perform.
  RegisterI18n(tlPhrases);
  return true;
}

bool cPluginCdda::Start(void)
{
  // Start any background activities the plugin shall perform.
  return true;
}

void cPluginCdda::Stop(void)
{
  // Stop any background activities the plugin shall perform.
}

void cPluginCdda::Housekeeping(void)
{
  // Perform any cleanup or other regular tasks.
}

cOsdObject *cPluginCdda::MainMenuAction(void)
{
  cCddaConfiguration& o_config = cCddaConfiguration::GetInstance();
  cCddaDevice& o_device = cCddaDevice::GetInstance();

  if (o_config.isAutoReplay()) {
    o_device.Lock();

    if (! o_device.isOpen())
      o_device.Open(o_config.GetDevice());

    if (o_device.isOpen() && o_device.Disc()->isAudio()) {
      cCddaCddb::GetInstance().Query(&o_device);

      unsigned int last = o_device.Disc()->Tracks()->GetFirst() +
                          o_device.Disc()->Tracks()->Count() - 1;
      cAudioPlaylist *po_playlist = new cAudioPlaylist(o_device.Disc(), "Instant", 0, true);
      for (unsigned int i = o_device.Disc()->Tracks()->GetFirst(); i <= last; i++) {
        cAudioTune *po_tune = new cAudioTune(po_playlist, i);
        if (NULL == po_playlist->Add(po_tune))
          delete po_tune;
      }
      po_playlist->SetCurrent(0);
      cControl::Launch(new cAudioControl(po_playlist));
    }

    o_device.Unlock();
    if (! o_device.isLocked())
      o_device.Close();

    return NULL;
  } else
    return new cCddaMenu();
}

cMenuSetupPage *cPluginCdda::SetupMenu(void)
{
  // Return a setup menu in case the plugin supports one.

  return new cCddaSetupMenu();
}

bool cPluginCdda::SetupParse(const char *Name, const char *Value)
{
  if (!strcasecmp(Name, "menuEntry"))
    cCddaConfiguration::GetInstance().SetMenuEntry(atoi(Value));
  else if (!strcasecmp(Name, "skipSeconds"))
    cCddaConfiguration::GetInstance().SetSkipLength(atoi(Value));
  else if (!strcasecmp(Name, "audioOnly"))
    cCddaConfiguration::GetInstance().SetAudioOnly(atoi(Value));
  else if (!strcasecmp(Name, "skipBackMargin"))
    cCddaConfiguration::GetInstance().SetSkipBackMargin(atoi(Value));
  else if (!strcasecmp(Name, "autoReplay"))
    cCddaConfiguration::GetInstance().SetAutoReplay(atoi(Value));
  else if (!strcasecmp(Name, "cddbServerFqdn"))
    cCddaConfiguration::GetInstance().SetCddbServerFqdn(Value);
  else if (!strcasecmp(Name, "cddbServerPort"))
    cCddaConfiguration::GetInstance().SetCddbServerPort(atoi(Value));
  else if (!strcasecmp(Name, "cddbPriority"))
    cCddaConfiguration::GetInstance().SetCddbPriority((eCddbPriority)atoi(Value));
  else if (!strcasecmp(Name, "cddbCache"))
    cCddaConfiguration::GetInstance().SetCddbCache(atoi(Value));
  else if (!strcasecmp(Name, "infoView"))
    cCddaConfiguration::GetInstance().SetDisplayInfoView((eDisplayInformationView)atoi(Value));
  else
    return false;

  return true;
}

VDRPLUGINCREATOR(cPluginCdda); // Don't touch this!
