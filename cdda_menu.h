/*
 * cdda_menu.h: The menu implementations
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda_menu.h,v 1.6 2005/12/15 12:45:07 cm Exp $
 */

#ifndef __CDDAMENU_H
#define __CDDAMENU_H

#include <vdr/osd.h>
#include <vdr/menu.h>
#include <vdr/menuitems.h>
#include "audio_playlist.h"

class cCddaMenu : public cOsdMenu {
  private:
    enum { ltSingle, ltAll , ltProgram } m_listType;
    cAudioPlaylist *mpo_playlist;
    cCddaDevice &o_device;
    void Initialize();
    void Set();
    void LoadPlaylist(int title);
  public:
    cCddaMenu();
    ~cCddaMenu();
    eOSState ProcessKey(eKeys Key);
};

class cCddaMenuCommands : public cOsdMenu {
  private:
    int m_track;
    cAudioPlaylist *mpo_playlist;
    void Set();
  public:
    cCddaMenuCommands(cAudioPlaylist *playlist, int track);
    ~cCddaMenuCommands();
    eOSState ProcessKey(eKeys Key);
};

class cCddaMenuTrackInfo : public cOsdMenu {
  private:
    const char *ConCatStr(const char *desc, const char *text, bool indent = false);
    void Set(cCddaDisc *disc, cCddaTrack *track);
  public:
    cCddaMenuTrackInfo(const char *Title, cCddaDisc *disc, cCddaTrack *track);
    virtual ~cCddaMenuTrackInfo();
};

class cCddaSetupMenu : public cMenuSetupPage {
  private:
    int m_newMenuEntry;
    int m_newSkipLength;
    int m_newAudioOnly;
    int m_newSkipBackMargin;
    int m_newAutoReplay;
    int m_newCddbCache;
    int m_newCddbPriority;
    char *mp_newCddbServerFqdn;
    int m_newCddbServerPort;
    int m_newInfoView;
    const char *m_cddbPrioTxt[3];
  protected:
    void Store(void);
  public:
    cCddaSetupMenu(void);
};

#endif
