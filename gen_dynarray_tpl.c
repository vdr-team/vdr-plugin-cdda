/*
 * gen_dynarray_tpl.c: The dynamic array template instantiations
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: gen_dynarray_tpl.c,v 1.1 2005/12/22 10:42:20 cm Exp $
 */

#ifndef __GEN_DYNARRAY_TPL_C
#define __GEN_DYNARRAY_TPL_C

#include "gen_dynarray.c"

// tell the compiler how we will use your class template
// YES, this is pretty ugly

#if defined(PLUGIN_DAAP)

#include "audio_playlist.h"
template class cDynamicArray<cAudioTune *>; // used in cAudioPlaylist
template class cDynamicArray<cAudioPlaylist *>; // used in cAudioPlaylists

#include "daap_client.h"
template class cDynamicArray<cDaapHost *>; // used in cDaapHosts
template class cDynamicArray<cDaapDatabase *>; // used in cDaapDatabases

#include "daap_database.h"
template class cDynamicArray<cMediaDatabaseGroup *>; // used in cMediaDatabaseEngine
template class cDynamicArray<cMediaDatabaseItem *>; // used in cMediaDatabaseEngine

#elif defined(PLUGIN_CDDA)

#include "audio_playlist.h"
template class cDynamicArray<cAudioTune *>; // used in cAudioPlaylist

#include "cdda_device.h"
template class cDynamicArray<cCddaTag *>; // used in cCddaTags

#endif // defined(PLUGIN_ ...)

#endif
