/*
 * gen_dynarray.h: The dynamic array interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: gen_dynarray.h,v 1.2 2005/12/21 09:01:45 cm Exp $
 */
 
#ifndef  __GENDYNARRAY_H
#define __GENDYNARRAY_H

template <typename ItemType>
class cDynamicArray {
  private:
    bool m_ownership;
    int m_itemBufferSize;
    bool Resize(void);
  protected:
    int m_numOfItems;
    ItemType *mp_items;
  public:
    // when ownership is false, then no object's will be delete'd/free'd when necessaray
    cDynamicArray(bool ownership = true);
    virtual ~cDynamicArray();
    //ItemType operator[](int index) const;
    bool hasOwnership() const { return m_ownership; };
    int Count(void) const { return m_numOfItems; };
    ItemType Item(int index) const;
    ItemType Add(ItemType item);
    int Delete(int index);
    int Move(int dest, int src);
    int Contains(ItemType item) const;
    bool Clear(void);
    void Pack(void);
};

#endif
