/*
 * cdda_conf.h: The configuration parameter storage
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda_conf.h,v 1.8 2005/12/16 09:49:41 cm Exp $
 */

#ifndef __CDDACONF_H
#define __CDDACONF_H

enum eCddbPriority { cpOff = 0, cpNoCdText, cpCacheOnly, cpAlways};

enum eDisplayInformationView { ivTrack = 0, ivAll };

char *NewStrDup(const char *string);
char *NewStrNDup(const char *string, int i);

class cCddaConfiguration {
  private:
    char *mp_device;
    char *mp_cddbDirectory;
    int m_skipLength;
    int m_skipBackMargin;
    bool m_menuEntry;
    bool m_audioOnly;
    bool m_autoReplay;
    bool m_cddbCache;
    char m_cddbServerFqdn[33];
    unsigned int m_cddbServerPort;
    eCddbPriority m_cddbPriority;
    eDisplayInformationView m_infoView;
    static cCddaConfiguration *gpo_config;
    cCddaConfiguration();
  public:
    ~cCddaConfiguration();
    static cCddaConfiguration& GetInstance(void);
    const char *GetDevice(void) const { return mp_device; };
    const char *SetDevice(const char *name);
    const char *GetCddbDir(void) const { return mp_cddbDirectory; };
    const char *SetCddbDir(const char *dir);
    int SetSkipLength(int seconds);
    int GetSkipLength(void) const { return m_skipLength; };
    int SetSkipBackMargin(int seconds);
    int GetSkipBackMargin(void) const {return m_skipBackMargin; };
    bool SetAudioOnly(bool on);
    bool isAudioOnly(void) const { return m_audioOnly; };
    bool SetMenuEntry(bool on);
    bool allowMenuEntry(void) const { return m_menuEntry; };
    bool SetAutoReplay(bool on);
    bool isAutoReplay(void) const { return m_autoReplay; };
    eCddbPriority SetCddbPriority(eCddbPriority priority);
    eCddbPriority GetCddbPriority(void) const { return m_cddbPriority; };
    const char *SetCddbServerFqdn(const char *fqdn);
    char *GetCddbServerFqdn(void) { return m_cddbServerFqdn; };
    int SetCddbServerPort(unsigned int port);
    int GetCddbServerPort(void) const { return m_cddbServerPort; };
    bool SetCddbCache(bool on);
    bool allowCddbCache(void) const { return m_cddbCache; };
    eDisplayInformationView SetDisplayInfoView(eDisplayInformationView view);
    eDisplayInformationView GetDisplayInfoView(void) const { return m_infoView; };
};

#endif
