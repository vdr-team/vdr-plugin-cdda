/*
 * cdda_i18n.c: The string translations
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda_i18n.c,v 1.12 2005/12/19 10:37:20 cm Exp $
 */

#include "cdda_i18n.h"

const tI18nPhrase tlPhrases[] = {
  { "CD Player",
    "CD Player",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Lecteur de CD",
    "",// TODO
    "CD-soitin",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "music cd player",
    "Musik CD Player",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Lecteur de CD Audio",
    "",// TODO
    "CD-soitin",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Mainmenu entry",
    "Men�eintrag",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Menu Principal",
    "",// TODO
    "Valinta p��valikossa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "show",
    "anzeigen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "voir",
    "",// TODO
    "n�yt�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "hide",
    "verstecken",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "cacher",
    "",// TODO
    "piilota",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Refresh",
    "Aktualisieren",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Rafraichir",
    "",// TODO
    "P�ivit�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Eject",
    "Auswerfen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Ejecter",
    "",// TODO
    "Avaa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Artist",
    "Interpret",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Artiste",
    "",// TODO
    "Esitt�j�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Track",
    "Titel",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Piste",
    "",// TODO
    "Kappale",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Title",
    "Titel",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Titre",
    "",// TODO
    "Nimi",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "No music cd in your drive",
    "Keine Musik CD eingelegt",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Pas de CD Audio dans le lecteur",
    "",// TODO
    "Asemassa ei ole CD-levy�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Jump to title",
    "Springe zu Titel",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Aller a",
    "",// TODO
    "Siirry",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "All",
    "Alle",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Toutes",
    "",// TODO
    "Kaikki",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Single",
    "",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Simple",
    "",// TODO
    "Valittu",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Skip length (s)",
    "Sprungweite (s)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Longueur de saut (s)",
    "",// TODO
    "Hypyn pituus (s)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Background",
    "Hintergrund",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Arri�re Plan",
    "",// TODO
    "Tausta",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Skip back margin (s)",
    "Sprunggrenze (s)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Marge saut arri�re (s)",
    "",// TODO
    "Hyppy kappaleen alkuun (s)",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "black",
    "schwarz",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "noir",
    "",// TODO
    "musta",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "live",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "vid�o",
    "",// TODO
    "live-kuva",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Composer",
    "Komponist",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Compositeur",
    "",// TODO
    "S�velt�j�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Songwriter",
    "Textdichter",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Parolier",
    "",// TODO
    "Sanoittaja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Arranger",
    "Bearbeiter",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Arrangeur",
    "",// TODO
    "Tuottaja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Genre",
    "",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Genre",
    "",// TODO
    "Tyylilaji",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Message",
    "Zusatztext",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Message",
    "",// TODO
    "Lis�tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Information",
    "",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Information",
    "",// TODO
    "Tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Cannot eject disc while playing",
    "Disk kann nicht ausgeworfen werden",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Ejection du disque impossible pendant la lecture",
    "",// TODO
    "Asemaa ei voida avata soitettaessa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Disc information",
    "Disk Informationen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Information du disque",
    "",// TODO
    "Levyn tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Track information",
    "Titel Informationen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Information de la piste",
    "",// TODO
    "Kappaleen tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Album",
    "Album",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Album",
    "",// TODO
    "Albumi",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Disc ID",
    "Disk ID",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "ID du disque",
    "",// TODO
    "Levyn tunniste",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Autostart replay",
    "Wiedergabe automatisch starten",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Lecture automatique",
    "",// TODO
    "Automaattinen toisto",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Program",
    "Programm",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Programme",
    "",// TODO
    "Ohjelmoidut",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Commands",
    "Befehle",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Commandes",
    "",// TODO
    "Komennot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Add to playlist",
    "Hinzuf�gen zur Spielliste",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Ajouter � la liste de lecture",
    "",// TODO
    "Lis�� soittolistalle",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Remove from playlist",
    "Entfernen von der Spielliste",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Supprimer de la liste de lecture",
    "",// TODO
    "Poista soittolistalta",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "CDDB Server FQDN",
    "CDDB Server FQDN",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Adresse du serveur CDDB",
    "",// TODO
    "CDDB-palvelimen osoite",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "CDDB Server Port",
    "CDDB Server Port",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Port du serveur CDDB",
    "",// TODO
    "CDDB-palvelimen portti",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "CDDB Priority",
    "CDDB Priorit�t",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Priorit� du serveur CDDB",
    "",// TODO
    "K�yt� CDDB-tietoja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "always",
    "immer",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "toujours",
    "",// TODO
    "aina",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "no cd-text",
    "kein CD-Text",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Pas de CD-Text",
    "",// TODO
    "ei CD-teksti�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "off",
    "aus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "�teint",
    "",// TODO
    "ei",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "on",
    "an",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "allum�",
    "",// TODO
    "kyll�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "CDDB Cache",
    "",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Cache du serveur CDDB",
    "",// TODO
    "K�yt� CDDB-v�limuistia",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Loading disc information",
    "Lade Disk Informationen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Chargement des informations du disque",// TODO
    "",// TODO
    "Ladataan levyn tietoja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Information view",
    "Informationsansicht",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Visualisation des informations",// TODO
    "",// TODO
    "N�yt� oletuksena seuraavat tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Disc",
    "Disk",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Levy",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Playlist",
    "Spielliste",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Soittolista",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "cache only",
    "nur Cache",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "vain v�limuisti",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Next disc/track tag",
    "N�chstes Disk/Track-Tag",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Vaihda levyn/kappaleen tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Only one disc/track tag available",
    "Nur ein Disk/Track-Tag verf�gbar",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Vain yhdet levyn/kappaleen tiedot saatavilla",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Delete current tag",
    "Aktuelles Tag l�schen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Poista nykyiset tiedot",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { "Cannot delete current tag",
    "Aktuelles Tag kann nicht gel�scht werden",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Nykyisi� tietoja ei voida poistaa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
  },
  { NULL }
};
