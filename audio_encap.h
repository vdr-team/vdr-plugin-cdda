/*
 * audio_encap.h: The audio encapsulator interface
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: audio_encap.h,v 1.1.1.1 2005/12/20 14:11:45 cm Exp $
 */

#ifndef __AUDIOENCAP_H
#define __AUDIOENCAP_H

#include <stdint.h>

const int PES_MAX_PACKSIZE = 2048;
const int PES_HEADER_LEN = 9;
const int PES_EXT_HEADER_LEN = 3;
const int LPCM_HEADER_LEN = 7;

class cEncapsulator {
  private:
    uint8_t m_pcmChannels;
    uint8_t m_pcmFrequency;
    uint8_t m_pesStreamId;
    uint16_t m_pesFlags;
    uint8_t m_buffer[PES_MAX_PACKSIZE];
    uint8_t *mp_offset;
  public:
    cEncapsulator();
    ~cEncapsulator();
    int Data(const uint8_t *payload, int length);
    void SetPcmParameters(uint8_t channels, uint16_t frequency);
    int ToPcm(uint8_t **packet);
    void SetPesParameters(uint8_t streamId, uint16_t flags);
    int ToPes(uint8_t **packet);
};

#endif
