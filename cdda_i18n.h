/*
 * cdda_i18n.h: The string translations
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda_i18n.h,v 1.1.1.1 2005/06/27 13:30:15 cm Exp $
 */


#ifndef __CDDAI18N_H
#define __CDDAI18N_H

#include <vdr/i18n.h>

extern const tI18nPhrase tlPhrases[];

#endif
