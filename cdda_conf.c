/*
 * cdda_conf.c: The configuration parameter storage
 *
 * See the README file for copyright information and how to reach the author.
 *
 * $Id: cdda_conf.c,v 1.6 2005/12/15 20:24:43 cm Exp $
 */

#include <stdlib.h>
#include <string.h>
#include "cdda_conf.h"

// --- NewStrDup ---------------------------------------------------------------

char *NewStrDup(const char *string) {
  if (NULL == string)
    return NULL;

  int n = strlen(string);
  char *p = new char[n + 1];
  strncpy(p, string, n);
  p[n] = 0x00;

  return p;
}

// --- NewStrNDup --------------------------------------------------------------

char *NewStrNDup(const char *string, int i) {
  if (NULL == string)
    return NULL;

  int n = strnlen(string, i);
  char *p = new char[n + 1];
  strncpy(p, string, n);
  p[n] = 0x00;

  return p;
}

// --- cConfiguration ---------------------------------------------------------
cCddaConfiguration::cCddaConfiguration() :
  mp_device(NULL), mp_cddbDirectory(NULL), m_skipLength(10), m_skipBackMargin(10),
  m_menuEntry(true), m_audioOnly(true), m_autoReplay(false), m_cddbCache(false),
  m_cddbServerPort(8880), m_cddbPriority(cpNoCdText), m_infoView(ivAll) {
  strncpy(m_cddbServerFqdn, "freedb.freedb.org", 33);
}

cCddaConfiguration::~cCddaConfiguration() {
  free(mp_device);
  free(mp_cddbDirectory);
}

cCddaConfiguration *cCddaConfiguration::gpo_config = NULL;

cCddaConfiguration& cCddaConfiguration::GetInstance(void) {
  if (NULL == gpo_config)
    gpo_config = new cCddaConfiguration();

  return *gpo_config;
}

const char *cCddaConfiguration::SetDevice(const char *name) {
  if (NULL != name) {
    free(mp_device);
    mp_device = strdup(name);
  }

  return name;
}

const char *cCddaConfiguration::SetCddbDir(const char *dir) {
  if (NULL != dir) {
    free(mp_cddbDirectory);
    mp_cddbDirectory = strdup(dir);
  }

  return dir;
}

int cCddaConfiguration::SetSkipLength(int seconds) {
  return m_skipLength = seconds;
}

int cCddaConfiguration::SetSkipBackMargin(int seconds) {
  return m_skipLength= seconds;
}

bool cCddaConfiguration::SetAudioOnly(bool on) {
  return m_audioOnly = on;
}

bool cCddaConfiguration::SetMenuEntry(bool on) {
  return m_menuEntry = on;
}

bool cCddaConfiguration::SetAutoReplay(bool on) {
  return m_autoReplay = on;
}

eCddbPriority cCddaConfiguration::SetCddbPriority(eCddbPriority priority) {
  return m_cddbPriority = priority;
}

const char *cCddaConfiguration::SetCddbServerFqdn(const char *fqdn) {
  return strncpy(m_cddbServerFqdn, fqdn, 33);
}

int cCddaConfiguration::SetCddbServerPort(unsigned int port) {
  return m_cddbServerPort = port;
}

bool cCddaConfiguration::SetCddbCache(bool on) {
  return m_cddbCache = on;
}

eDisplayInformationView cCddaConfiguration::SetDisplayInfoView(eDisplayInformationView view) {
  return m_infoView = view;
}
